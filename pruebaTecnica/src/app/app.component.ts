import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { clienteDTO } from './modelo/clienteDTO';
import { facturaDTO } from './modelo/facturaDTO';
import { productoDTO } from './modelo/productoDTO';
import { ServicioGlobal } from './services/servicio-global.service' 

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  facturaForm:FormGroup;
  clienteForm:FormGroup;
  productoForm:FormGroup;
  factura = new facturaDTO();
  cliente = new clienteDTO();
  producto = new productoDTO();
  dialogCliente = false;
  dialogProducto = false;

  constructor(
    public fb:FormBuilder,
    public servicioGlobal:ServicioGlobal
  ){
  }


  ngOnInit():void{

    this.facturaForm = this.fb.group({
      fecha:[''],
      cantidad:[''],
      precio:[''],
    })

    this.productoForm = this.fb.group({
      nombre:[''],
      precio:[''],
      stock:[''],
    })

    this.clienteForm = this.fb.group({
      nombre:[''],
      apellido:[''],
      direccion:[''],
      fechaNacimiento:[''],
      telefono:[''],
      email:[''],
    })
   
  }

  registrarProducto(){
    this.dialogProducto = true;
  }

  registrarCliente(){
    this.dialogCliente = true;
  }

  cerrarDialogo(){
    this.dialogProducto = false;
    this.dialogCliente = false;
  }



  guardar():void{

    this.producto = this.productoForm.value;
    this.cliente = this.clienteForm.value;
    this.factura = this.facturaForm.value;
    this.factura.producto = this.producto;
    this.factura.cliente = this.cliente;
    
    this.servicioGlobal.guardarCliente(this.factura).subscribe(resp =>{

    },
    error => {console.log(error)}
    );
  }
}
