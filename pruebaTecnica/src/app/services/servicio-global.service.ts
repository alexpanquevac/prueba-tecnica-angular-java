import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { facturaDTO } from '../modelo/facturaDTO';

@Injectable({
  providedIn: 'root'
})
export class ServicioGlobal {

  private API_SERVER = 'http://localhost:8080/global';

  constructor( private http:HttpClient) { }


  public guardarCliente(factura:facturaDTO):Observable<facturaDTO>{

    return this.http.post<facturaDTO>(this.API_SERVER+"/cliente",factura);
  }
}
