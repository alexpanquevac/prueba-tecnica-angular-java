import { clienteDTO } from "./clienteDTO";
import { productoDTO } from "./productoDTO";

export class facturaDTO {
    numFactura: number;
    fecha: string;
    cantidad: number;
    precio: string;
    cliente: clienteDTO;
    producto: productoDTO;
}