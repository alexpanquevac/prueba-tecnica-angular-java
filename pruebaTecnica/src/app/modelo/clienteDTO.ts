export class clienteDTO {
    idCliente: number;
    nombre: string;
    apellido: string;
    direccion: string;
    fechaNacimiento: string;
    telefono: number;
    email: string;
}