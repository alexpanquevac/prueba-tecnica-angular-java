import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {ButtonModule} from 'primeng/button';
import {DialogModule} from 'primeng/dialog';
import { CalendarModule } from 'primeng/calendar';
import {InputTextModule} from 'primeng/inputtext';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    HttpClientModule,
    ButtonModule,
    DialogModule,BrowserAnimationsModule,
    CalendarModule,
    InputTextModule
  ],
    providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
