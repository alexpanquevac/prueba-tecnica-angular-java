package com.app.pruebaTecnicaService.respository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.app.pruebaTecnicaService.modelo.Cliente; 
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Alexander
 */
@Repository
public interface ClienteRespositorio extends JpaRepository<Cliente, Integer> {
    
}
