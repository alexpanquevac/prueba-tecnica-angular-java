/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.pruebaTecnicaService.respository;

import com.app.pruebaTecnicaService.modelo.Factura;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Alexander
 */
@Repository
public interface FacturaRepositorio extends JpaRepository<Factura, Integer>{
    
}
