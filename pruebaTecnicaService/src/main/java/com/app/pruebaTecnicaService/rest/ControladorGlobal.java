/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.pruebaTecnicaService.rest;

import com.app.pruebaTecnicaService.DTO.FacturaDTO;
import com.app.pruebaTecnicaService.DTO.TransferDTO;
import java.net.URI;
import com.app.pruebaTecnicaService.modelo.Cliente;
import com.app.pruebaTecnicaService.modelo.Detalle;
import com.app.pruebaTecnicaService.modelo.DetallePk;
import com.app.pruebaTecnicaService.modelo.Factura;
import com.app.pruebaTecnicaService.modelo.Producto;
import com.app.pruebaTecnicaService.service.ServicioGlobal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Alexander
 */
@RestController
@RequestMapping("/global/")
public class ControladorGlobal {

    @Autowired
    @Qualifier("globalService")
    private ServicioGlobal servicioGlobal;

    @PostMapping("cliente")
    private ResponseEntity<FacturaDTO> guardarCliente(@RequestBody FacturaDTO factura) {
        
        TransferDTO transferir = new TransferDTO();
        Cliente clienteTemp = servicioGlobal.crearCliente(transferir.clienteBD(factura.getCliente()));
        Producto producto = servicioGlobal.crearProducto(transferir.productoBD(factura.getProducto()));
        factura.getCliente().setIdCliente(clienteTemp.getIdCliente());
        Factura facturaBD = servicioGlobal.crearFactura(transferir.facturaBD(factura));
        DetallePk pk = new DetallePk();
        Detalle detalle = new Detalle();
        pk.setId_factura(facturaBD.getNumFactura());
        detalle.setId(pk);
        detalle.setCantidad(factura.getCantidad());
        detalle.setPrecio(factura.getPrecio());
        detalle.setIdProducto(producto.getIdProducto());
        servicioGlobal.crearDetalle(detalle);
        
        FacturaDTO facturaTemp = new FacturaDTO();
        System.out.println(factura);
        try {
            return ResponseEntity.created(new URI("/api/persona")).body(facturaTemp);

        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
    }

}
