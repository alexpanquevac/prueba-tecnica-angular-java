package com.app.pruebaTecnicaService.service;

import com.app.pruebaTecnicaService.modelo.Cliente;
import com.app.pruebaTecnicaService.modelo.Detalle;
import com.app.pruebaTecnicaService.modelo.Factura;
import com.app.pruebaTecnicaService.modelo.Producto;
import com.app.pruebaTecnicaService.respository.ClienteRespositorio;
import com.app.pruebaTecnicaService.respository.DetalleRepositorio;
import com.app.pruebaTecnicaService.respository.FacturaRepositorio;
import com.app.pruebaTecnicaService.respository.ProductoRespositorio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Alexander
 */
@Service("globalService")
public class ServicioGlobal {

    @Autowired
    private ClienteRespositorio clienteRepositorio;

    @Autowired
    private ProductoRespositorio productoRepositorio;

    @Autowired
    private FacturaRepositorio facturaRepositorio;

    @Autowired
    private DetalleRepositorio detalleRepositorio;

    public Cliente crearCliente(Cliente cliente) {
        return clienteRepositorio.save(cliente);
    }

    public Producto crearProducto(Producto producto) {
        return productoRepositorio.save(producto);
    }
    
    public Detalle crearDetalle(Detalle detalle){
        return detalleRepositorio.save(detalle);
    }
    
    public Factura crearFactura(Factura factura){
        return facturaRepositorio.save(factura);
    }

}
