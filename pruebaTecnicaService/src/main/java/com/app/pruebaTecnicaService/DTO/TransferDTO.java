/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.pruebaTecnicaService.DTO;

import com.app.pruebaTecnicaService.modelo.Cliente;
import com.app.pruebaTecnicaService.modelo.Detalle;
import com.app.pruebaTecnicaService.modelo.DetallePk;
import com.app.pruebaTecnicaService.modelo.Factura;
import com.app.pruebaTecnicaService.modelo.Producto;

/**
 *
 * @author Alexander
 */
public class TransferDTO {
    
    public Cliente clienteBD(ClienteDTO clienteDTO){
        Cliente clienteBD = new Cliente();
        clienteBD.setNombre(clienteDTO.getNombre());
        clienteBD.setApellido(clienteDTO.getApellido());
        clienteBD.setDireccion(clienteDTO.getDireccion());
        clienteBD.setFechaNacimiento(clienteDTO.getFechaNacimiento());
        clienteBD.setTelefono(clienteDTO.getTelefono());
        clienteBD.setEmail(clienteDTO.getEmail());
        
        return clienteBD;
    }
    
    public Producto productoBD(ProductoDTO productoDTO){
        Producto productoBD = new Producto();
        productoBD.setIdProducto(productoDTO.getIdProducto());
        productoBD.setNombre(productoDTO.getNombre());
        productoBD.setPrecio(productoDTO.getPrecio());
        productoBD.setStock(productoDTO.getStock());
        
        return productoBD;
    }
    
    public Factura facturaBD(FacturaDTO facturaDTO){
        Factura facturaBD = new Factura();
        facturaBD.setFecha(facturaDTO.getFecha());
        facturaBD.setIdCliente(facturaDTO.getCliente().getIdCliente());
        facturaBD.setNumFactura(facturaDTO.getNumFactura());
        return facturaBD;
    }
    
    public Detalle detalleBD(DetalleDTO detalleDTO) {
        Detalle detalleBD = new Detalle();
        DetallePk pk = new DetallePk();
        pk.setId_factura(detalleDTO.getIdFactura());
        pk.setNum_detalle(detalleDTO.getNumDetalle());
        detalleBD.setId(pk);
        detalleBD.setCantidad(detalleDTO.getCantidad());
        detalleBD.setIdProducto(detalleDTO.getIdProducto());
        detalleBD.setPrecio(detalleDTO.getPrecio());
        return detalleBD;
    }
    
}
