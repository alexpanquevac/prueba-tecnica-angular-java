/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.app.pruebaTecnicaService.modelo;

import java.io.Serializable;
import javax.persistence.Embeddable;

/**
 *
 * @author Alexander
 */

@Embeddable
public class DetallePk implements Serializable{
    
    
    private int num_detalle;
    private int id_factura;

    public int getNum_detalle() {
        return num_detalle;
    }

    public void setNum_detalle(int num_detalle) {
        this.num_detalle = num_detalle;
    }

    public int getId_factura() {
        return id_factura;
    }

    public void setId_factura(int id_factura) {
        this.id_factura = id_factura;
    }
    
    
    
    
}
